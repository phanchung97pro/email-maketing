/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { view: 'pages/homepage' },
  // Users
  'POST /api/register': 'UsersController.register',
  'POST /api/login': 'UsersController.login',
  'DELETE /api/logout': 'UsersController.logout',
  'PUT /api/users/update': 'UsersController.update',
  'PUT /api/users/updatePassword': 'UsersController.updatePassword',
  'DELETE /api/users/delete': 'UsersController.delete',

  // Contact Forder
  'GET /api/forder/select': 'ContactForderController.select',
  'POST /api/forder/create': 'ContactForderController.create',
  'PUT /api/forder/update': 'ContactForderController.update',
  'DELETE /api/forder/delete': 'ContactForderController.delete',
  // Contact list
  'GET /api/list/select': 'ContactListController.select',
  'POST /api/list/create': 'ContactListController.create',
  'PUT /api/list/update': 'ContactListController.update',
  'DELETE /api/list/delete': 'ContactListController.delete',
  // Contact Customer
  'GET /api/customer/select': 'ContactCustomerController.select',
  'POST /api/customer/create': 'ContactCustomerController.create',
  'PUT /api/customer/update': 'ContactCustomerController.update',
  'DELETE /api/customer/delete': 'ContactCustomerController.delete',
  // Upload Customer
  'POST /api/customer/upload': 'UploadCustomerController.upload',

  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
