/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {
  /***************************************************************************
   *                                                                          *
   * Default policy for all controllers and actions, unless overridden.       *
   * (`true` allows public access)                                            *
   *                                                                          *
   ***************************************************************************/
  UsersController: {
    update: "isAuthenticated",
    logout: "isAuthenticated",
    updatePassword: "isAuthenticated",
    delete: ["isAuthenticated", "isAdmin"],
  },
  ContactForderController: {
    select: "isAuthenticated",
    create: "isAuthenticated",
    update: "isAuthenticated",
    delete: "isAuthenticated",
  },
  ContactListController: {
    select: "isAuthenticated",
    create: "isAuthenticated",
    update: "isAuthenticated",
    delete: "isAuthenticated",
  },
  ContactCustomerController: {
    select: "isAuthenticated",
    create: "isAuthenticated",
    update: "isAuthenticated",
    delete: "isAuthenticated",
  },
  UploadCustomerController: {
    upload: "isAuthenticated",
  },
};
