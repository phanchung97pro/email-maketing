module.exports = (req, res, next) => {
  console.log(req.user);
  if (req.user.rules === 0) {
    return next();
  } else {
    res.status(404).send({
      status: 'error',
      message: 'You are not the admin',
    });
  }
};
