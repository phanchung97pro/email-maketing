module.exports = {
  Error: function (ErrorCategories) {
    switch (ErrorCategories) {
      case 'ID_NOT_NULL': {
        return {
          message: 'ID NOT NULL',
          code: 400,
        };
      }
      case 'ID_NOT_EXIST': {
        return {
          message: 'ID NOT EXIST',
          code: 400,
        };
      }
      case 'USERNAME_NOT_NULL': {
        return {
          message: 'USERNAME NOT NULL',
          code: 400,
        };
      }
      case 'USERNAME_INVALID': {
        return {
          message: 'USERNAME INVALID',
          code: 400,
        };
      }
      case 'USERNAME_NOT_EXIST': {
        return {
          message: 'USERNAME NOT EXIST',
          code: 400,
        };
      }
      case 'PASSWORD_NOT_NULL': {
        return {
          message: 'PASSWORD NOT NULL',
          code: 400,
        };
      }
      case 'PASSWORD_NOT_LENGTH': {
        return {
          message: 'PASSWORD MIN: 5, MAX: 20',
          code: 400,
        };
      }
      case 'PASSWORD_NOT_EXIST': {
        return {
          message: 'PASSWORD NOT EXIST',
          code: 400,
        };
      }
      case 'NAME_NOT_NULL': {
        return {
          message: 'NAME NOT NULL',
          code: 400,
        };
      }
      case 'NAME_NOT_LENGTH': {
        return {
          message: 'NAME MIN: 5, MAX: 20',
          code: 400,
        };
      }
      case 'EMAIL_NOT_NULL': {
        return {
          message: 'EMAIL NOT NULL',
          code: 400,
        };
      }
      case 'PHONE_NOT_NULL': {
        return {
          message: 'PHONE NOT NULL',
          code: 400,
        };
      }
      case 'PHONE_INVALID': {
        return {
          message: 'PHONE INVALID',
          code: 400,
        };
      }
      case 'ADDRESS_NOT_NULL': {
        return {
          message: 'ADDRESS NOT NULL',
          code: 400,
        };
      }
      case 'ADDRESS_NOT_LENGTH': {
        return {
          message: 'ADDRESS MIN: 5, MAX: 20',
          code: 400,
        };
      }
      case 'RULES_NOT_NULL': {
        return {
          message: 'RULES NOT NULL',
          code: 400,
        };
      }
      case 'STATUS_NOT_NULL': {
        return {
          message: 'STATUS NOT EXIST',
          code: 400,
        };
      }
      case 'STATUS_NOT_EXIST': {
        return {
          message: 'STATUS NOT EXIST',
          code: 400,
        };
      }
      case 'CHECK_USERS': {
        return {
          message: 'USERS OR EMAIL ALREADY EXIST',
          code: 400,
        };
      }
      case 'EMAIL_ALREADY_EXIST': {
        return {
          message: 'EMAIL ALREADY EXIST',
          code: 400,
        };
      }
      case 'UPDATE_NOT_SUCCESS': {
        return {
          message: 'UPDATE NOT SUCCESS, PLEASE CHECK THE INFORMATION AGAIN',
          code: 400,
        };
      }
      default: {
        return {
          message: 'ERROR SERVER',
          code: 500,
        };
      }
    }
  }
};

