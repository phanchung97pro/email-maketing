module.exports = {
  Error: function (ErrorContacts) {
    switch (ErrorContacts) {
      case "LIMIT_NOT_FOUND": {
        return {
          message: "LIMIT NOT FOUND",
          code: 404,
        };
      }
      case "OFFSET_NOT_FOUND": {
        return {
          message: "OFFSET NOT FOUND",
          code: 404,
        };
      }
      case "LIMIT_OR_OFFSET_NOT_NUMBER": {
        return {
          message: "LIMIT OR OFFSET NOT NUMBER",
          code: 404,
        };
      }
      case "RECORD_NOT_FOUND": {
        return {
          message: "RECORD NOT FOUND",
          code: 404,
        };
      }
      case "ID_NOT_NULL": {
        return {
          message: "ID NOT NULL",
          code: 404,
        };
      }
      case "ID_NOT_FOUND": {
        return {
          message: "ID NOT FOUND",
          code: 404,
        };
      }
      case "ID_NOT_ARRAY": {
        return {
          message: "ID NOT ARRAY",
          code: 404,
        };
      }
      case "FORDER_NOT_NULL": {
        return {
          message: "FORDER NOT NULL",
          code: 404,
        };
      }
      case "FORDER_NOT_LENGTH": {
        return {
          message: "FORDER MIN: 5, MAX: 50",
          code: 404,
        };
      }
      case "FORDER_NOT_EXIST": {
        return {
          message: "FORDER NOT EXIST",
          code: 404,
        };
      }
      case "FORDER_ALREADY_EXIST": {
        return {
          message: "FORDER ALREADY EXIST",
          code: 404,
        };
      }
      case "LIST_NOT_NULL": {
        return {
          message: "LIST NOT NULL",
          code: 404,
        };
      }
      case "LIST_NOT_LENGTH": {
        return {
          message: "LIST MIN: 5, MAX: 50",
          code: 404,
        };
      }
      case "LIST_ALREADY_EXIST": {
        return {
          message: "LIST ALREADY EXIST",
          code: 404,
        };
      }
      case "LIST_NOT_EXIST": {
        return {
          message: "LIST NOT EXIST",
          code: 404,
        };
      }
      case "LIST_ID_NOT_NUMBER": {
        return {
          message: "LIST_ID NOT NUMBER",
          code: 404,
        };
      }
      case "EMAIL_NOT_NULL": {
        return {
          message: "EMAIL NOT NULL",
          code: 404,
        };
      }
      case "EMAIL_INVALID": {
        return {
          message: "EMAIL INVALID",
          code: 404,
        };
      }
      case "EMAIL_NOT_LENGTH": {
        return {
          message: "EMAIL MAX: 100",
          code: 404,
        };
      }
      case "EMAIL_ALREADY_EXIST": {
        return {
          message: "EMAIL ALREADY EXIST",
          code: 404,
        };
      }
      case "UPDATE_NOT_SUCCESS": {
        return {
          message: "UPDATE NOT SUCCESS",
          code: 404,
        };
      }
      case "DELETE_NOT_SUCCESS": {
        return {
          message: "DELETE NOT SUCCESS",
          code: 404,
        };
      }
      default: {
        return {
          message: "ERROR SERVER",
          code: 500,
        };
      }
    }
  },
};
