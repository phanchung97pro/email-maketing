module.exports = {
  // Register Users
  Register: async function (req, res) {
    try {
      let { username, password, name, email, phone, address, rules, status } = req.body;
      let register = await UsersServices.Register(username, password, name, email, phone, address, rules, status);
      console.log(register);
      return res.json({
        status: 'success',
        message: 'register success',
        code: 200,
        data: {
          token: register[0],
          users: register[1]
        }
      });
    } catch (error) {
      console.log(error);
      if (error.raw) {
        return res.status(400).json({
          status: 'error',
          ...error.raw
        });
      } else {
        return res.status(400).json({
          status: 'error',
          ...error
        });
      }
    }
  },

  // Login Users
  Login: async function (req, res) {
    try {
      var username = req.body.username;
      var password = req.body.password;
      var login = await UsersServices.Login(username, password);
      return res.json({
        status: 'success',
        message: 'login success',
        code: 200,
        data: {
          token: login[0],
          users: login[1]
        }
      });
    } catch (error) {
      console.log(error);
      return res.json({
        status: 'error',
        ...error,
      });
    }
  },

  //LogOut Users
  logOut: async function (req, res) {
    try {
      let token = req.token;
      console.log(token);
      let logout = await JWT.destroy({ JWT: token }).fetch();
      return res.json({
        status: 'success',
        message: 'login success',
        code: 200,
        data: logout
      });
    } catch (error) {
      console.log(error);
      return res.json({
        status: 'error',
        ...error,
      });
    }
  },

  // update Users
  update: async function (req, res) {
    try {
      let { name, email, phone, address } = req.body;
      let user = req.user;
      console.log(user);
      var login = await UsersServices.update(name, email, phone, address, user);
      return res.json({
        status: 'success',
        message: 'update success',
        code: 200,
        data: login
      });
    } catch (error) {
      console.log(error);
      if (error.raw) {
        return res.status(400).json({
          status: 'error',
          ...error.raw
        });
      } else {
        return res.status(400).json({
          status: 'error',
          ...error
        });
      }
    }
  },

  // update Password

  updatePassword: async function (req, res) {
    try {
      let password = req.body.password;
      let user = req.user;
      var updatePassword = await UsersServices.updatePassword(password, user);
      return res.json({
        status: 'success',
        message: 'update password success',
        code: 200,
        data: updatePassword
      });
    } catch (error) {
      console.log(error);
      if (error.raw) {
        return res.status(400).json({
          status: 'error',
          ...error.raw
        });
      } else {
        return res.status(400).json({
          status: 'error',
          ...error
        });
      }
    }
  },

  // Delete Password
  delete: async function (req, res) {
    try {
      let ids = req.body.id;
      let deleteUsers = await UsersServices.delete(ids);
      return res.json({
        status: 'success',
        message: 'delete success',
        code: 200,
        data: deleteUsers
      });
    } catch (error) {
      console.log(error);
      return res.json({
        status: 'error',
        ...error,
      });
    }
  },
};

