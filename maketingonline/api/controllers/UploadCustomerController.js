let moment = require("moment");
const XLSX = require("xlsx");
const path = require("path");
module.exports = {
  upload: async function (req, res) {
    try {
      let forder = req.body.forder;
      let list = req.body.list;
      let user = req.user;
      let check = await UploadCustomerServices.upload(forder, list, user);
      // console.log("sssssssss");
      // console.log(check);
      req.file("file").upload(
        {
          dirname: require("path").resolve(sails.config.appPath, "upload"),
        },
        async function (err, uploadedFiles) {
          if (err) {
            return res.serverError(err);
          }

          console.log(uploadedFiles.length);
          if (uploadedFiles.length === 0) {
            return res.json({
              status: "error",
              code: 404,
              message: "NO FILE WAS UPLOADED",
            });
          }
          console.log(uploadedFiles);
          let editfile = uploadedFiles[0].fd.split("maketingonline")[1];
          console.log(editfile);
          let string = "maketingOnline" + editfile;
          console.log(string);
          var workbook = XLSX.readFile(
            path.join(__dirname + `../../../../${string}`),
            { type: "binary" }
          );
          let result = XLSX.utils.sheet_to_json(
            workbook.Sheets[workbook.SheetNames[0]]
          );
          console.log(result);
          // console.log(result[0].email);
          if (!result[0].email) {
            return res.json({
              status: "error",
              code: 404,
              message: "WRONG EXCEL FORMAT",
            });
          }
          let createfile = [];
          for (let i = 0; i < result.length; i++) {
            //console.log(result[i].email);
            if (!result[i].email || result[i].email.trim() === "") {
              return res.json({
                status: "error",
                code: 404,
                message: "EMAIL NOT NULL",
                data: result[i],
              });
            }
            let checkEmail = await ContactCustomer.findOne({
              email: result[i].email,
              list_id: check,
            });
            if (!checkEmail) {
              createfile[i] = await ContactCustomer.createEach([
                {
                  email: result[i].email,
                  name: result[i].name,
                  phone: result[i].phone,
                  address: result[i].address,
                  list_id: check,
                  createdAt: moment.utc().format(),
                  updatedAt: moment.utc().format(),
                },
              ]).fetch();
            }
            if (checkEmail) {
              // console.log(result[i].email);
              createfile[i] = await ContactCustomer.update({
                email: result[i].email,
                list_id: check,
              })
                .set({
                  name: result[i].name,
                  phone: result[i].phone,
                  address: result[i].address,
                  updatedAt: moment.utc().format(),
                })
                .fetch();
            }
          }
          //console.log(createfile);
          if (createfile.length != 0) {
            return res.json({
              status: "success",
              message: " upload success",
              data: createfile,
            });
          }
        }
      );
    } catch (error) {
      console.log(error);
      return res.json({
        status: "error",
        ...error,
      });
    }
  },
};
