/**
 * ContactCustomerController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  select: async function (req, res) {
    try {
      let filed = req.param("filed");
      let limit = req.param("limit");
      let offset = req.param("offset");
      let user = req.user;
      let selectList = await ContactCustomerServices.select(
        filed,
        limit,
        offset,
        user
      );
      return res.json({
        status: "success",
        code: "200",
        data: selectList,
      });
    } catch (error) {
      console.log(error);
      return res.json({
        status: "error",
        ...error,
      });
    }
  },

  create: async function (req, res) {
    try {
      let { email, name, phone, address, list_id } = req.body;
      let user = req.user;
      let createEmail = await ContactCustomerServices.create(
        email,
        name,
        phone,
        address,
        list_id,
        user
      );
      return res.json({
        status: "success",
        code: 200,
        message: "them thanh cong",
        data: createEmail,
      });
    } catch (error) {
      console.log(error);
      return res.json({
        status: "error",
        ...error,
      });
    }
  },

  update: async function (req, res) {
    try {
      let id = req.param("id");
      let { email, name, phone, address } = req.body;
      let user = req.user;
      let updateEmail = await ContactCustomerServices.update(
        id,
        email,
        name,
        phone,
        address,
        user
      );
      return res.json({
        status: "success",
        code: 200,
        message: "update thanh cong",
        data: updateEmail,
      });
    } catch (error) {
      console.log(error);
      return res.json({
        status: "error",
        ...error,
      });
    }
  },

  delete: async function (req, res) {
    try {
      let id = req.body.id;
      let user = req.user;
      let deleteEmail = await ContactCustomerServices.delete(id, user);
      return res.json({
        status: "success",
        code: 200,
        message: "Delete thanh cong",
        data: deleteEmail,
      });
    } catch (error) {
      console.log(error);
      return res.json({
        status: "error",
        ...error,
      });
    }
  },
};
