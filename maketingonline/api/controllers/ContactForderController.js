/**
 * ContactForderController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  select: async function (req, res) {
    try {
      let filed = req.param("filed");
      let limit = req.param("limit");
      let offset = req.param("offset");
      let user = req.user;
      let selectForder = await ContactForderServices.select(
        filed,
        limit,
        offset,
        user
      );
      return res.json({
        status: "success",
        code: "200",
        data: selectForder,
      });
    } catch (error) {
      console.log(error);
      return res.json({
        status: "error",
        ...error,
      });
    }
  },
  create: async function (req, res) {
    try {
      let forder = req.body.forder;
      let user = req.user;
      console.log(user);
      let createContactForder = await ContactForderServices.create(
        forder,
        user
      );
      return res.json({
        status: "success",
        code: 200,
        message: "them thanh cong",
        data: createContactForder,
      });
    } catch (error) {
      console.log(error);
      return res.json({
        status: "error",
        ...error,
      });
    }
  },
  update: async function (req, res) {
    try {
      let id = req.param("id");
      let forder = req.body.forder;
      let user = req.user;
      let updateContactForder = await ContactForderServices.update(
        id,
        forder,
        user
      );
      console.log(updateContactForder);
      return res.json({
        status: "success",
        code: 200,
        message: "update thanh cong",
        data: updateContactForder,
      });
    } catch (error) {
      console.log(error);
      return res.json({
        status: "error",
        ...error,
      });
    }
  },
  delete: async function (req, res) {
    try {
      let id = req.body.id;
      let user = req.user;
      let deleteForder = await ContactForderServices.delete(id, user);
      return res.json({
        status: "success",
        code: 200,
        message: "Delete thanh cong",
        data: deleteForder,
      });
    } catch (error) {
      console.log(error);
      return res.json({
        status: "error",
        ...error,
      });
    }
  },
};
