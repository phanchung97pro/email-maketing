/**
 * ContactListController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  select: async function (req, res) {
    try {
      let filed = req.param("filed");
      let limit = req.param("limit");
      let offset = req.param("offset");
      let user = req.user;
      let selectList = await ContactListServices.select(
        filed,
        limit,
        offset,
        user
      );
      return res.json({
        status: "success",
        code: "200",
        data: selectList,
      });
    } catch (error) {
      console.log(error);
      return res.json({
        status: "error",
        ...error,
      });
    }
  },
  create: async function (req, res) {
    try {
      let list = req.body.list;
      let forder = req.body.forder;
      let user = req.user;
      let createList = await ContactListServices.create(list, forder, user);
      return res.json({
        status: "success",
        code: 200,
        message: "them thanh cong",
        data: createList,
      });
    } catch (error) {
      console.log(error);
      return res.json({
        status: "error",
        ...error,
      });
    }
  },
  update: async function (req, res) {
    try {
      let id = req.param("id");
      let list = req.body.list;
      let user = req.user;
      let updateList = await ContactListServices.update(id, list, user);
      return res.json({
        status: "success",
        code: 200,
        message: "update thanh cong",
        data: updateList,
      });
    } catch (error) {
      console.log(error);
      return res.json({
        status: "error",
        ...error,
      });
    }
  },
  delete: async function (req, res) {
    try {
      let id = req.body.id;
      let user = req.user;
      let deleteList = await ContactListServices.delete(id, user);
      return res.json({
        status: "success",
        code: 200,
        message: "Delete thanh cong",
        data: deleteList,
      });
    } catch (error) {
      console.log(error);
      return res.json({
        status: "error",
        ...error,
      });
    }
  },
};
