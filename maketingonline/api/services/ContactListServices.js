let moment = require("moment");
let LISTS = require("../constants/ErrorContact");
module.exports = {
  select: async function (filed, limit, offset, user) {
    if (filed === undefined) {
      filed = String(filed);
      filed = "";
    }
    if (limit === undefined) {
      throw LISTS.Error("LIMIT_NOT_FOUND");
    }
    if (offset === undefined) {
      throw LISTS.Error("OFFSET_NOT_FOUND");
    }
    if (limit.trim() === "") {
      limit = 10;
    }
    if (offset === "") {
      offset = 0;
    }
    if (isNaN(limit) || isNaN(offset)) {
      throw LISTS.Error("LIMIT_OR_OFFSET_NOT_NUMBER");
    }
    let sql =
      "SELECT contact_list.id,contact_list.list,contact_list.forder_id,contact_list.createdAt,contact_list.updatedAt,contact_forder.id AS id_forder,contact_forder.forder,contact_forder.user_id AS user_foder,contact_forder.createdAt AS createdAt_forder,contact_forder.updatedAt AS updatedAt_forder FROM contact_list LEFT JOIN contact_forder ON contact_list.forder_id = contact_forder.id WHERE user_id=$1 AND contact_list.list LIKE $2 LIMIT $3 OFFSET $4";
    let selectList = await sails.sendNativeQuery(sql, [
      user.id,
      "%" + filed + "%",
      parseInt(limit),
      parseInt(offset),
    ]);
    //console.log(selectList);
    if (selectList.rows.length === 0) {
      throw LISTS.Error("RECORD_NOT_FOUND");
    }
    return selectList.rows;
  },

  create: async function (list, forder, user) {
    if (!list || list.trim() === "") {
      throw LISTS.Error("LIST_NOT_NULL");
    }
    if (list.length < 5 || list.length > 50) {
      throw LISTS.Error("LIST_NOT_LENGTH");
    }
    if (!forder || forder.trim() === "") {
      throw LISTS.Error("FORDER_NOT_NULL");
    }
    let checkforder = await ContactForder.findOne({
      forder: forder,
      user_id: user.id,
    });
    console.log(checkforder);
    if (!checkforder) {
      throw LISTS.Error("FORDER_NOT_EXIST");
    }
    // let sql =
    //   "SELECT contact_list.list FROM contact_list INNER JOIN contact_forder ON contact_list.forder_id = contact_forder.id INNER JOIN users ON contact_forder.user_id = users.id WHERE users.id = $1";
    // let checklists = await sails.sendNativeQuery(sql, [user.id]);
    // console.log(checklists.rows);
    // let checklist = checklists.rows;
    // for (let i = 0; i < checklist.length; i++) {
    //   console.log(checklist[i].list);
    //   if (list === checklist[i].list) {
    //     throw LISTS.Error("LIST_ALREADY_EXIST");
    //   }
    // }
    let checklist = await ContactList.findOne({
      list: list,
      forder_id: checkforder.id,
    });
    if (checklist) {
      throw LISTS.Error("LIST_ALREADY_EXIST");
    }

    let createlist = await ContactList.create({
      list: list,
      forder_id: checkforder.id,
      createdAt: moment.utc().format(),
      updatedAt: moment.utc().format(),
    }).fetch();
    return createlist;
  },

  update: async function (id, list, user) {
    if (!id || id === "") {
      throw LISTS.Error("ID_NOT_NULL");
    }
    if (!list || list.trim() === "") {
      throw LISTS.Error("LIST_NOT_NULL");
    }
    if (list.length < 5 || list.length > 50) {
      throw LISTS.Error("LIST_NOT_LENGTH");
    }
    let checkid = await ContactList.findOne({
      id: id,
    });
    if (!checkid) {
      throw LISTS.Error("ID_NOT_FOUND");
    }
    let findfoder = await ContactForder.findOne({
      user_id: user.id,
      id: checkid.forder_id,
    });
    console.log(findfoder);
    if (!findfoder) {
      throw LISTS.Error("FORDER_NOT_EXIST");
    }
    let checklist = await ContactList.findOne({
      list: list,
      forder_id: findfoder.id,
    });
    if (checklist && checklist.id != id) {
      throw LISTS.Error("LIST_ALREADY_EXIST");
    }

    let updateList = await ContactList.updateOne({
      id: id,
      forder_id: findfoder.id,
    }).set({
      list: list,
      forder_id: findfoder.id,
      updatedAt: moment.utc().format(),
    });
    return updateList;
  },
  delete: async function (id, user) {
    if (!id || id === "") {
      throw LISTS.Error("ID_NOT_NULL");
    }
    if (Array.isArray(id) == false) {
      throw LISTS.Error("ID_NOT_ARRAY");
    }
    let checkid = await ContactList.find({
      id: { in: id },
    });
    if (checkid.length == 0) {
      throw LISTS.Error("ID_NOT_FOUND");
    }
    let sql =
      "SELECT contact_list.forder_id FROM contact_list INNER JOIN contact_forder ON contact_list.forder_id = contact_forder.id INNER JOIN users ON contact_forder.user_id = users.id WHERE users.id = $1";
    let checkUser = await sails.sendNativeQuery(sql, [user.id]);
    //console.log(checkUser.rows);
    let checkUsers = checkUser.rows;
    let forder_id = [];
    for (let i = 0; i < checkUsers.length; i++) {
      console.log(checkUsers[i].forder_id);
      forder_id[i] = checkUsers[i].forder_id;
    }
    let deleteList = await ContactList.destroy({
      id: { in: id },
      forder_id: { in: forder_id },
    }).fetch();
    if (deleteList.length === 0) {
      throw LISTS.Error("DELETE_NOT_SUCCESS");
    }
    return deleteList;
  },
};
