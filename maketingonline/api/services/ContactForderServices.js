let moment = require("moment");
let CONTACT = require("../constants/ErrorContact");
module.exports = {
  select: async function (filed, limit, offset, user) {
    if (filed === undefined) {
      filed = String(filed);
      filed = "";
    }
    if (limit === undefined) {
      throw CONTACT.Error("LIMIT_NOT_FOUND");
    }
    if (offset === undefined) {
      throw CONTACT.Error("OFFSET_NOT_FOUND");
    }
    if (limit.trim() === "") {
      limit = 10;
    }
    if (offset === "") {
      offset = 0;
    }
    if (isNaN(limit) || isNaN(offset)) {
      throw CONTACT.Error("LIMIT_OR_OFFSET_NOT_NUMBER");
    }
    let sql =
      "SELECT contact_forder.id,contact_forder.forder,contact_forder.user_id,contact_forder.createdAt,contact_forder.updatedAt FROM contact_forder WHERE user_id=$1 AND contact_forder.forder LIKE $2 LIMIT $3 OFFSET $4";
    let selectForder = await sails.sendNativeQuery(sql, [
      user.id,
      "%" + filed + "%",
      parseInt(limit),
      parseInt(offset),
    ]);
    if (selectForder.rows.length === 0) {
      throw CONTACT.Error("RECORD_NOT_FOUND");
    }
    return selectForder.rows;
  },
  create: async function (forder, user) {
    if (!forder || forder.trim() === "") {
      throw CONTACT.Error("FORDER_NOT_NULL");
    }
    if (forder.length < 5 || forder.length > 50) {
      throw CONTACT.Error("FORDER_NOT_LENGTH");
    }
    let checkforder = await ContactForder.findOne({
      forder: forder,
      user_id: user.id,
    });
    if (checkforder) {
      throw CONTACT.Error("FORDER_NOT_EXIST");
    }
    let createForder = await ContactForder.create({
      forder: forder,
      user_id: user.id,
      createdAt: moment.utc().format(),
      updatedAt: moment.utc().format(),
    }).fetch();
    return createForder;
  },
  update: async function (id, forder, user) {
    if (!id || id === "") {
      throw CONTACT.Error("ID_NOT_NULL");
    }
    if (!forder || forder.trim() === "") {
      throw CONTACT.Error("FORDER_NOT_NULL");
    }
    if (forder.length < 5 || forder.length > 50) {
      throw CONTACT.Error("FORDER_NOT_LENGTH");
    }
    let checkid = await ContactForder.findOne({
      id: id,
    });
    if (!checkid) {
      throw CONTACT.Error("ID_NOT_FOUND");
    }
    let checkforder = await ContactForder.findOne({
      forder: forder,
      user_id: user.id,
    });
    if (checkforder && id != checkforder.id) {
      throw CONTACT.Error("FORDER_ALREADY_EXIST");
    }
    let updateForder = await ContactForder.updateOne({
      id: id,
      user_id: user.id,
    }).set({
      forder: forder,
      user_id: user.id,
      updatedAt: moment.utc().format(),
    });
    if (!updateForder) {
      throw CONTACT.Error("UPDATE_NOT_SUCCESS");
    }
    return updateForder;
  },
  delete: async function (id, user) {
    if (!id || id === "") {
      throw CONTACT.Error("ID_NOT_NULL");
    }
    if (Array.isArray(id) == false) {
      throw CONTACT.Error("ID_NOT_ARRAY");
    }
    let checkid = await ContactForder.find({
      id: { in: id },
    });
    console.log(checkid.length);
    if (checkid.length == 0) {
      throw CONTACT.Error("ID_NOT_FOUND");
    }
    let deleteForder = await ContactForder.destroy({
      id: { in: id },
      user_id: user.id,
    }).fetch();
    if (deleteForder.length === 0) {
      throw CONTACT.Error("DELETE_NOT_SUCCESS");
    }
    return deleteForder;
  },
};
