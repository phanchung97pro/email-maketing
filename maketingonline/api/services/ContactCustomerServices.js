let moment = require("moment");
let CUSTOMERS = require("../constants/ErrorContact");
module.exports = {
  select: async function (filed, limit, offset, user) {
    if (filed === undefined) {
      filed = String(filed);
      filed = "";
    }
    if (limit === undefined) {
      throw CUSTOMERS.Error("LIMIT_NOT_FOUND");
    }
    if (offset === undefined) {
      throw CUSTOMERS.Error("OFFSET_NOT_FOUND");
    }
    if (limit.trim() === "") {
      limit = 10;
    }
    if (offset === "") {
      offset = 0;
    }
    if (isNaN(limit) || isNaN(offset)) {
      throw CUSTOMERS.Error("LIMIT_OR_OFFSET_NOT_NUMBER");
    }
    let sql =
      "SELECT contact_customer.id,contact_customer.email,contact_customer.`name`,contact_customer.phone,contact_customer.address,contact_customer.list_id,contact_customer.createdAt,contact_customer.updatedAt,contact_list.id AS id_list,contact_list.list FROM contact_customer LEFT JOIN contact_list ON contact_customer.list_id = contact_list.id LEFT JOIN contact_forder ON contact_list.forder_id = contact_forder.id WHERE user_id=$1 AND contact_customer.email LIKE $2 LIMIT $3 OFFSET $4";
    let selectCustomer = await sails.sendNativeQuery(sql, [
      user.id,
      "%" + filed + "%",
      parseInt(limit),
      parseInt(offset),
    ]);
    if (selectCustomer.rows.length === 0) {
      throw CUSTOMERS.Error("RECORD_NOT_FOUND");
    }
    return selectCustomer.rows;
  },

  create: async function (email, name, phone, address, list_id, user) {
    if (!list_id || list_id === "") {
      throw CUSTOMERS.Error("LIST_NOT_NULL");
    }
    if (!email || email === "") {
      throw CUSTOMERS.Error("EMAIL_NOT_NULL");
    }
    if (!/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(email)) {
      throw CUSTOMERS.Error("EMAIL_INVALID");
    }
    if (email.length > 100) {
      throw CUSTOMERS.Error("EMAIL_NOT_LENGTH");
    }
    if (isNaN(list_id)) {
      throw CUSTOMERS.Error("LIST_ID_NOT_NUMBER");
    }
    let sql =
      "SELECT users.id  FROM contact_list INNER JOIN contact_forder ON contact_list.forder_id = contact_forder.id INNER JOIN users ON contact_forder.user_id = users.id WHERE contact_list.id = $1";
    let checkUsers = await sails.sendNativeQuery(sql, [list_id]);
    console.log(checkUsers);
    if (checkUsers.rows.length === 0 || checkUsers.rows[0].id !== user.id) {
      throw CUSTOMERS.Error("LIST_NOT_EXIST");
    }
    let checkmail = await ContactCustomer.findOne({
      email: email,
      list_id: list_id,
    });
    if (checkmail) {
      throw CUSTOMERS.Error("EMAIL_ALREADY_EXIST");
    }

    let createEmail = await ContactCustomer.create({
      email: email,
      name: name,
      phone: phone,
      address: address,
      list_id: list_id,
      createdAt: moment.utc().format(),
      updatedAt: moment.utc().format(),
    }).fetch();
    console.log(createEmail);
    return createEmail;
  },

  update: async function (id, email, name, phone, address, user) {
    if (!id || id === "") {
      throw CUSTOMERS.Error("ID_NOT_NULL");
    }
    if (!email || email === "") {
      throw CUSTOMERS.Error("EMAIL_NOT_NULL");
    }
    if (!/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(email)) {
      throw CUSTOMERS.Error("EMAIL_INVALID");
    }
    if (email.length > 100) {
      throw CUSTOMERS.Error("EMAIL_NOT_LENGTH");
    }
    let checkid = await ContactCustomer.findOne({
      id: id,
    });
    if (!checkid) {
      throw CUSTOMERS.Error("ID_NOT_FOUND");
    }
    let sql =
      "SELECT users.id FROM contact_customer INNER JOIN contact_list ON contact_customer.list_id = contact_list.id INNER JOIN contact_forder ON contact_list.forder_id = contact_forder.id INNER JOIN users ON contact_forder.user_id = users.id WHERE contact_customer.list_id = $1";
    let checkUsers = await sails.sendNativeQuery(sql, [checkid.list_id]);
    if (checkUsers.rows[0].id !== user.id) {
      throw CUSTOMERS.Error("LIST_NOT_EXIST");
    }
    let checkmail = await ContactCustomer.findOne({
      email: email,
      list_id: checkid.list_id,
    });
    //console.log(checkid.list_id);
    if (checkmail && checkmail.id != id) {
      throw CUSTOMERS.Error("EMAIL_ALREADY_EXIST");
    }
    let updateEmail = await ContactCustomer.updateOne({
      id: id,
    }).set({
      email: email,
      name: name,
      phone: phone,
      address: address,
      updatedAt: moment.utc().format(),
    });
    return updateEmail;
  },

  delete: async function (id, user) {
    if (!id || id === "") {
      throw CUSTOMERS.Error("ID_NOT_NULL");
    }
    if (Array.isArray(id) == false) {
      throw CUSTOMERS.Error("ID_NOT_ARRAY");
    }
    let checkid = await ContactCustomer.find({
      id: { in: id },
    });
    if (checkid.length == 0) {
      throw CUSTOMERS.Error("ID_NOT_FOUND");
    }
    let sql =
      "SELECT contact_customer.list_id FROM contact_customer INNER JOIN contact_list ON contact_customer.list_id = contact_list.id INNER JOIN contact_forder ON contact_list.forder_id = contact_forder.id INNER JOIN users ON contact_forder.user_id = users.id WHERE users.id = $1";
    let checkUser = await sails.sendNativeQuery(sql, [user.id]);
    //console.log(selectUser.rows);
    let checkUsers = checkUser.rows;
    let list_id = [];
    for (let i = 0; i < checkUsers.length; i++) {
      console.log(checkUsers[i].list_id);
      list_id[i] = checkUsers[i].list_id;
    }
    let deleteList = await ContactCustomer.destroy({
      id: { in: id },
      list_id: { in: list_id },
    }).fetch();
    if (deleteList.length === 0) {
      throw CUSTOMERS.Error("DELETE_NOT_SUCCESS");
    }
    return deleteList;
  },
};
