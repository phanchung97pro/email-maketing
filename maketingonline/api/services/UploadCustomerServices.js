let moment = require("moment");
let CUSTOMERS = require("../constants/ErrorContact");
module.exports = {
  upload: async function (forder, list, user) {
    if (!forder || forder.trim() === "") {
      throw CUSTOMERS.Error("FORDER_NOT_NULL");
    }
    if (forder.length < 5 || forder.length > 50) {
      throw CUSTOMERS.Error("FORDER_NOT_LENGTH");
    }
    if (!list || list.trim() === "") {
      throw CUSTOMERS.Error("LIST_NOT_NULL");
    }
    if (list.length < 5 || list.length > 50) {
      throw CUSTOMERS.Error("LIST_NOT_LENGTH");
    }
    //ktra forder co ton tai hay khong,neu khong thi tao moi
    let checkforder = await ContactForder.findOne({
      forder: forder,
      user_id: user.id,
    });
    let id_list;
    if (!checkforder) {
      let createForder = await ContactForder.create({
        forder: forder,
        user_id: user.id,
        createdAt: moment.utc().format(),
        updatedAt: moment.utc().format(),
      }).fetch();
      let createlist = await ContactList.create({
        list: list,
        forder_id: createForder.id,
        createdAt: moment.utc().format(),
        updatedAt: moment.utc().format(),
      }).fetch();
      id_list = createlist.id;
    }

    if (checkforder) {
      //ktra list co ton tai hay khong,neu khong thi tao moi
      let checklist = await ContactList.findOne({
        list: list,
        forder_id: checkforder.id,
      });
      if (!checklist) {
        let createlist = await ContactList.create({
          list: list,
          forder_id: checkforder.id,
          createdAt: moment.utc().format(),
          updatedAt: moment.utc().format(),
        }).fetch();
        id_list = createlist.id;
      }
      if (checklist) {
        id_list = checklist.id;
      }
    }
    return id_list;
  },
};
