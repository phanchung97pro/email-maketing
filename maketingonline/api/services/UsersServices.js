const bcrypt = require('bcrypt');
let jwt = require('jsonwebtoken');
let moment = require('moment');
const JWT_KEY = 'adwdadnwundkamdiwdkamdiw';
let USERS = require('../constants/ErrorUsers');
module.exports = {
  // Register
  Register: async function (username, password, name, email, phone, address, rules, status) {
    if (!username || username.trim() === '') {
      throw USERS.Error('USERNAME_NOT_NULL');
    }
    if (!/^[A-Za-z0-9_\.]{6,30}$/.test(username)) {
      throw USERS.Error('USERNAME_INVALID');
    }

    if (!password || password.trim() === '') {
      throw USERS.Error('PASSWORD_NOT_NULL');
    }
    if (password.length < 5 || password.length > 20) {
      throw USERS.Error('PASSWORD_NOT_LENGTH');
    }

    if (!name || name.trim() === '') {
      throw USERS.Error('NAME_NOT_NULL');
    }
    if (name.length < 5 || name.length > 50) {
      throw USERS.Error('NAME_NOT_LENGTH');
    }

    if (!email || email.trim() === '') {
      throw USERS.Error('EMAIL_NOT_NULL');
    }
    if (!/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(email)) {
      throw USERS.Error('EMAIL_INVALID');
    }

    if (!phone || phone.trim() === '') {
      throw USERS.Error('PHONE_NOT_NULL');
    }
    if (!/(09|08|07)+([0-9]{8})\b/.test(phone)) {
      throw USERS.Error('PHONE_INVALID');
    }

    if (!address || address.trim() === '') {
      throw USERS.Error('ADDRESS_NOT_NULL');
    }
    if (address.length > 255) {
      throw USERS.Error('ADDRESS_NOT_LENGTH');
    }

    if (!rules || rules.trim() === '') {
      throw USERS.Error('RULES_NOT_NULL');
    }

    if (!status || status.trim() === '') {
      throw USERS.Error('STATUS_NOT_NULL');
    }

    let register = await Users.create({
      username: username,
      password: password,
      name: name,
      email: email,
      phone: phone,
      address: address,
      rules: rules,
      status: status,
      createdAt: moment.utc().format(),
      updatedAt: moment.utc().format(),
    }).intercept('E_UNIQUE', () => { throw USERS.Error('CHECK_USERS'); }).fetch();

    const token = jwt.sign(
      {
        id: register.id,
        username: register.username,
        name: register.name,
        email: register.email,
        phoneNumber: register.phone,
        address: register.address,
        rules: register.rules,
        status: register.status
      },
      JWT_KEY,
      { expiresIn: '7d' },
    );

    createJWT = await JWT.create({
      JWT: token,
      createdAt: moment.utc().format(),
      updatedAt: moment.utc().format(),
    }).fetch();

    let DeleteJWT = await setTimeout(async (token) => {
      let deleteJWT = await Jwt.destroy({
        Jwt: token,
      }).fetch();
      return deleteJWT;
    }, 604800000);

    verifyToken = jwt.verify(token, JWT_KEY);

    return [token, verifyToken, DeleteJWT];
  },

  // Login
  Login: async function (username, password) {
    if (!username || username.trim() === '') {
      throw USERS.Error('USERNAME_NOT_NULL');
    }
    if (!password || password.trim() === '') {
      throw USERS.Error('PASSWORD_NOT_NULL');
    }

    let login = await Users.findOne({
      where: {
        username: username,
      },
    });
    if (!login) {
      throw USERS.Error('USERNAME_NOT_EXIST');
    }
    let data = bcrypt.compareSync(password, login.password);
    // console.log(data)
    if (!data) {
      throw USERS.Error('PASSWORD_NOT_EXIST');
    }
    const token = jwt.sign(
      {
        id: login.id,
        username: login.username,
        name: login.name,
        email: login.email,
        phoneNumber: login.phone,
        address: login.address,
        rules: login.rules,
        status: login.status,
      },
      JWT_KEY,
      { expiresIn: '7d' },
    );
    verifyToken = jwt.verify(token, JWT_KEY);
    createJWT = await JWT.create({
      JWT: token,
      createdAt: moment.utc().format(),
      updatedAt: moment.utc().format(),
    }).fetch();

    let DeleteJWT = await setTimeout(async (token) => {
      let deleteJWT = await Jwt.destroy({
        Jwt: token,
      }).fetch();
      return deleteJWT;
    }, 604800000);

    return [token, verifyToken, DeleteJWT];
  },

  // logOut Users
  logOut: async function (token) {
    let deleteJWT = await JWT.destroy({ JWT: token });
    return deleteJWT;
  },

  // update Users
  update: async function (name, email, phone, address, user) {
    console.log(user.id);
    if (!name || name.trim() === '') {
      throw USERS.Error('NAME_NOT_NULL');
    }
    if (name.length < 5 || name.length > 50) {
      throw USERS.Error('NAME_NOT_LENGTH');
    }

    if (!email || email.trim() === '') {
      throw USERS.Error('EMAIL_NOT_NULL');
    }
    if (!/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/.test(email)) {
      throw USERS.Error('EMAIL_INVALID');
    }

    if (!phone || phone.trim() === '') {
      throw USERS.Error('PHONE_NOT_NULL');
    }
    if (!/(09|08|07)+([0-9]{8})\b/.test(phone)) {
      throw USERS.Error('PHONE_INVALID');
    }

    if (!address || address.trim() === '') {
      throw USERS.Error('ADDRESS_NOT_NULL');
    }
    if (address.length > 255) {
      throw USERS.Error('ADDRESS_NOT_LENGTH');
    }
    let updatedUser = await Users.updateOne({
      id: user.id
    }).set({
      name: name,
      email: email,
      phone: phone,
      address: address,
      updatedAt: moment.utc().format(),
    });

    console.log(updatedUser);

    if (!updatedUser) {
      throw USERS.Error('UPDATE_NOT_SUCCESS');
    }
    return updatedUser;
  },

  // update Password
  updatePassword: async function (password, user) {
    console.log(user.id);
    if (!password || password.trim() === '') {
      throw USERS.Error('PASSWORD_NOT_NULL');
    }
    if (password.length < 5 || password.length > 20) {
      throw USERS.Error('PASSWORD_NOT_LENGTH');
    }

    let updatedPassword = await Users.updateOne({
      id: user.id
    }).set({
      password: bcrypt.hashSync(password, 10),
      updatedAt: moment.utc().format(),
    });

    console.log(updatedPassword);

    if (!updatedPassword) {
      throw USERS.Error('UPDATE_NOT_SUCCESS');
    }
    return updatedPassword;
  },

  // delete
  delete: async function (ids) {
    if (!ids || ids === '') {
      throw USERS.Error('ID_NOT_NULL');
    }
    if (Array.isArray(ids) === false) {
      throw USERS.Error('ID_IS_ARRAY');
    }

    let checkId = await Users.find({
      select: ['id'],
      where: {
        id: { in: ids },
      },
    });
    if (checkId.length === 0) {
      throw USERS.Error('ID_NOT_EXIST');
    }
    let deleteUsers = await Categories.destroy({ id: { in: ids } }).fetch();
    return deleteUsers;
  },
};
