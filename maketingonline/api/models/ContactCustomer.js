/**
 * ContactCustomer.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: "contact_customer",
  attributes: {
    email: {
      type: "string",
      columnType: "varchar(50)",
      required: true,
      maxLength: 100,
    },
    name: {
      type: "string",
      columnType: "varchar(50)",
      maxLength: 50,
      minLength: 3,
    },
    phone: {
      type: "string",
      columnType: "varchar(15)",
    },
    address: {
      type: "string",
      columnType: "varchar",
      maxLength: 255,
    },
    list_id: {
      type: "number",
      columnType: "int(11)",
    },
  },
};
