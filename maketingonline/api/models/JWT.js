module.exports = {
  tableName: 'jwt',
  attributes: {
    JWT: {
      type: 'string',
      columnType: 'text',
      required: true,
    },
  },
};
