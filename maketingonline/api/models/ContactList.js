/**
 * ContactList.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: "contact_list",
  attributes: {
    list: {
      type: "string",
      columnType: "varchar(50)",
      maxLength: 50,
      minLength: 5,
    },
    forder_id: {
      type: "number",
      columnType: "int(11)",
    },
  },
};
