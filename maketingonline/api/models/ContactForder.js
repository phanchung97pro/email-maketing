/**
 * ContactForder.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  tableName: "contact_forder",
  attributes: {
    forder: {
      type: "string",
      columnType: "varchar(50)",
      maxLength: 50,
      minLength: 5,
    },
    user_id: {
      type: "number",
      columnType: "int(11)",
    },
  },
};
