const bcrypt = require('bcrypt');
module.exports = {
  tableName: 'Users',
  attributes: {
    username: {
      type: 'string',
      columnType: 'varchar(50)',
      required: true,
      unique: true,
      maxLength: 20,
      minLength: 5,
    },

    password: {
      type: 'string',
      required: true,
    },

    name: {
      type: 'string',
      columnType: 'varchar(50)',
      required: true,
      maxLength: 50,
      minLength: 5,
    },

    email: {
      type: 'string',
      columnType: 'varchar(50)',
      required: true,
      unique: true,
      maxLength: 100,
    },

    phone: {
      type: 'string',
      columnType: 'varchar(15)',
      required: true,
    },

    address: {
      type: 'string',
      columnType: 'varchar',
      required: true,
      maxLength: 255,
    },

    rules: {
      type: 'number',
      columnType: 'int(1)',
    },

    status: {
      type: 'string',
      columnType: 'varchar(50)',
      isIn: ['PENDING', 'ACTIVE', 'DE-ACTIVE'],
      defaultsTo: 'PENDING',
    },
  },
  beforeCreate: function (users, cb) {
    bcrypt.genSalt(10, (error, salt) => {
      bcrypt.hash(users.password, salt, (error, hash) => {
        if (error) {
          console.log(err);
          return cb(error);
        } else {
          users.password = hash;
          return cb();
        }
      });
    });
  },
};
