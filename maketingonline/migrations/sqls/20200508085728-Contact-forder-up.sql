/* Replace with your SQL commands */
CREATE TABLE `contact_forder`
(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forder` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11),
   `createdAt` timestamp NULL DEFAULT NULL ON
  UPDATE current_timestamp(),
  `updatedAt` timestamp NULL DEFAULT NULL ON
  UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  -- UNIQUE KEY `forder` (`forder`),
  CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;