CREATE TABLE `users`
(
  `id` int
(11) NOT NULL AUTO_INCREMENT,
  `username` varchar
(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar
(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar
(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar
(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar
(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar
(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rules` int
(1) DEFAULT NULL,
  `status` varchar
(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` timestamp NULL DEFAULT NULL ON
UPDATE current_timestamp(),
  `updatedAt` timestamp NULL DEFAULT NULL ON
UPDATE current_timestamp(),
  PRIMARY KEY
(`id`),
  UNIQUE KEY `id`
(`id`),
  UNIQUE KEY `username`
(`username`),
  UNIQUE KEY `email`
(`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
