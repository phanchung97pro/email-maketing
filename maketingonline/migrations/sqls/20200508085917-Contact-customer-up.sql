/* Replace with your SQL commands */
CREATE TABLE `contact_customer`
(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
   `phone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `list_id` int(11),
   `createdAt` timestamp NULL DEFAULT NULL ON
  UPDATE current_timestamp(),
  `updatedAt` timestamp NULL DEFAULT NULL ON
  UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  -- UNIQUE KEY `email` (`email`),
  CONSTRAINT fk_list FOREIGN KEY (list_id) REFERENCES contact_list(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;