CREATE TABLE `statistical_total_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaingn_id` int(11) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `total_read_email` int(11) DEFAULT NULL,
  `total_not_read_email` int(11) DEFAULT NULL,
  `total_click_email` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT fk_group5 FOREIGN KEY (campaingn_id) REFERENCES campaingns(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;