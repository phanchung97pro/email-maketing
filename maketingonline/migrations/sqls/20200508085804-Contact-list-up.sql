/* Replace with your SQL commands */
CREATE TABLE `contact_list`
(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forder_id` int(11),
   `createdAt` timestamp NULL DEFAULT NULL ON
  UPDATE current_timestamp(),
  `updatedAt` timestamp NULL DEFAULT NULL ON
  UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  -- UNIQUE KEY `list` (`list`),
  CONSTRAINT fk_forder FOREIGN KEY (forder_id) REFERENCES contact_forder(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;