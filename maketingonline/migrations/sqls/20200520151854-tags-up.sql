CREATE TABLE `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(11) NOT NULL,
  `campaingn_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT fk_group1 FOREIGN KEY (list_id) REFERENCES Contact_list(id),
  CONSTRAINT fk_group2 FOREIGN KEY (campaingn_id) REFERENCES campaingns(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
