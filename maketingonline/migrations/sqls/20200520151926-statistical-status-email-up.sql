CREATE TABLE `statistical_status_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaingn_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `status` enum('click email','read email','not read email') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT fk_group3 FOREIGN KEY (campaingn_id) REFERENCES campaingns(id),
  CONSTRAINT fk_group4 FOREIGN KEY (customer_id) REFERENCES contact_customer(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;